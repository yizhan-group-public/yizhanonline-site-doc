# 需求 `need.py`
## solutionneed_detail：方案服务详情页面
- <http://www.yizhanonline.com/needs/solutionneed/detail?id=1>
    - 出错跳转首页

## solutionneed_update：提交方案服务
- 在方案服务页面点击提交方案需求按钮
    - 方法：`POST`
    - 需要有企业用户登录
    - 出错跳转方案服务页面

## artistneed_detail：找艺人需求详情页面
- <http://www.yizhanonline.com/needs/artistneed/detail?id=1>
    - 方法：`GET`
    - 可公开访问

## artistneed_edit：新建/修改找艺人需求页面
- 新建：<http://www.yizhanonline.com/needs/artistneed/edit>
- 修改：<http://www.yizhanonline.com/needs/artistneed/edit?id=1>
    - 方法：`GET`
    - 需要有企业用户登录
    - 企业用户只可以修改属于自己的找艺人需求
    - 出错跳转首页

## artistneed_update：
- 在新建/修改找艺人需求页面点击提交艺人需求按钮
    - 方法：`POST`
    - 需要有企业用户登录
    - 企业用户只可以修改属于自己的找艺人需求
    - 默认跳转找艺人需求详情页面
    - 出错跳转新建/修改找艺人需求页面
    - 更新、新增是原子操作
