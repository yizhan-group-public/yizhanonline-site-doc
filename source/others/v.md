# Views（templates）
> 位置：`/main/templates/*`

## 艺人 `artist/*`
> 存放所有与艺人相关的模版

### dashboard.html：个人中心
- <http://www.yizhanonline.com/artist/dashboard/>

### detail.html：艺人信息
- <http://www.yizhanonline.com/artist/detail?id=1>

### login.html：艺人登录/注册页面
- <http://www.yizhanonline.com/artist/login>

### navbar.html：艺人页面使用的导航条
```
{% include "artist/navbar.html" %}
```

### profile.html：艺人完善资料页面
- <http://www.yizhanonline.com/artist/profile/>

******

## 企业 `enterprise/*`
> 存放所有企业相关的模版

### detail.html：企业详情页面
- <http://www.yizhanonline.com/enterprise/detail?id=1>

### login.html：企业登陆/注册页面
- <http://www.yizhanonline.com/enterprise/login>

******

## 首页 `index/*`
> 存放首页及首页用的导航条

### index.html：网站首页
- <http://www.yizhanonline.com>

### navbar.html：首页用导航条
```
{% include "index/navbar.html" %}
```

******

## 活动 `job/*`
> 存放所有活动相关的模版

### apply.html：活动报名页面
- <http://www.yizhanonline.com/job/apply?id=1>

### applydetail.html：活动报名详情
- <http://www.yizhanonline.com/job/applydetail?id=1>

### applylist.html：活动报名列表
- <http://www.yizhanonline.com/job/applylist?id=1>

### collaborator.html：活动合作机构模版
```
{% include "job/collaborator.html" %}
```

### detail.html：活动详情页面
- <http://www.yizhanonline.com/job/detail?id=1>

### edit.html：新建/编辑活动页面
- 新建：<http://www.yizhanonline.com/job/edit>
- 编辑：<http://www.yizhanonline.com/job/edit?id=2>

### list.html：最新活动页面
- <http://www.yizhanonline.com/job/list/>

### manage.html：管理活动页面
- <http://www.yizhanonline.com/job/manage>

******

## 需求 `needs/*`
> 存放所有需求表单相关的模版

### artistneed/detail.html：找艺人需求详情
- <http://www.yizhanonline.com/needs/artistneed/detail?id=1>

### artistneed/edit.html：新建/修改找艺人需求
- 新建：<http://www.yizhanonline.com/needs/artistneed/edit>
- 修改：<http://www.yizhanonline.com/needs/artistneed/edit?id=1>

### solutionneed/detail.html：方案服务详情
- <http://www.yizhanonline.com/needs/solutionneed/detail?id=1>

******

## 独立页面 `singlepage/*`
> 包含一些只有 1-2 页不适合分类的页面

### contactus.html：联系我们页面
- <http://www.yizhanonline.com/contactus/>

### map.html：公司地址地图模块
- 嵌入 contactus.html 中

### qcyzj.html：青春艺绽季活动页面
- <http://www.yizhanonline.com/qcyzj/>

### qcyzjadd.html：青春艺绽季报名成功页面
- 在青春艺绽季活动页面报名后自动出现

### qcyzjlist.html：查看已报名人员列表
- <http://www.yizhanonline.com/qcyzjlist?password=y****n>

### solutionservice.html：方案服务页面
- <http://www.yizhanonline.com/solutionservice/>

### supportservice.html：配套服务页面
- <http://www.yizhanonline.com/supportservice/>

******

## 测试 `test/*`
> 网站测试页面

### message.html
- <http://www.yizhanonline.com/test/message/>
- 用于测试 message 模版是否可用，以及确定样式和显示级别

******

## 不再使用的页面
### `support/*`
- 曾经的配套服务页面，现已挪入 singlepage/supportservice.html

### `industry/*`
- 曾经的行业登陆页面，现已不再使用。

### `develop/*`
- 开发时使用的页面，目前均不再有用处。
