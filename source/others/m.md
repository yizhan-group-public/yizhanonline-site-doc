# 数据层 `Models.py`
## 艺人
### class Artist
> 存放艺人用户的基本属性、帐号密码。

### class ArtistFile
> 存放艺人上传的视频与图片，不包括头像
- 一个 Artist 可有 0 到多个 ArtistFile

******

## 企业
### class Enterprise
> 存放企业用户的基本属性、帐号密码。

******

## 活动
### class Job
> 存放活动的基本属性，包括海报

### class JobQuestion
> 存放活动的自定义问题题面
- 一个 Job 可有 0 到多个 JobQuestion

### class JobApply
> 存放活动的艺人报名信息
- 一个 JobApply 对应一个 Job 和 一个 Artist
- 一个 Job 对应 0 到多个 JobApply

### class JobApplyAnswer
> 存放活动自定义问题的回答
- 一个 JobApplyAnswer 对应一个 JobQuestion
- 一个 JobApply 对应多个 JobApplyAnswer

******

## 需求表单
### class SolutionNeed
> 存放方案服务表单数据
- <http://www.yizhanonline.com/solutionservice/>

### class ArtistNeed
> 存放找艺人表单数据的基本信息
- <http://www.yizhanonline.com/needs/artistneed/edit>

### class ArtistNeedTotalPrice
> 存放找艺人表单数据中“项目总价”的信息
- 根据 ArtistNeed.budgetttype == 'total' 来判断

### class ArtistNeedUnitPrice
> 存放找艺人表单数据中“人员单价”的信息
- 根据 ArtistNeed.budgetttype == 'unit' 来判断
- 一个 ArtistNeed 可有 1 到多个 ArtistNeedUnitPrice

******

## 青春艺绽季
### class QcyzjArtist
> 存放“青春艺绽季”活动报名的艺人的基本信息

### class QcyzjArtistData
> 存放“青春艺绽季”活动报名的艺人的文件信息
- 分为视频和图片，但理论上存任何文件皆可
