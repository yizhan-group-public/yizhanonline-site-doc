# 独立页面 `singlepage.py`
## solution_service：方案服务页面
- <http://www.yizhanonline.com/solutionservice/>
    - 需要有企业用户登录

## support_service：配套服务页面
- <http://www.yizhanonline.com/supportservice/>
    - 可公开访问

## contact_us：联系我们页面
- <http://www.yizhanonline.com/contactus/>
    - 可公开访问

## qcyzj：青春艺绽季活动页面
- <http://www.yizhanonline.com/qcyzj/>
    - 可公开访问
    - 会在 memcache 中增加 pv，并读取至页面隐藏

## qcyzj_add：青春艺绽季报名成功页面
- 在青春艺绽季活动页面报名后自动出现
    - 方法：`POST`
    - 出错跳转青春艺绽季活动页面

## qcyzj_list：查看已报名人员列表
- <http://www.yizhanonline.com/qcyzjlist?password=y****n>
    - 方法：`GET`
    - 出错跳转青春艺绽季活动页面
    - 有弱密码保护，可公开访问
