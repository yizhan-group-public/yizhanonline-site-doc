## 活动 `job.py`
> 活动相关的业务逻辑

### list：最新活动页面
- <http://www.yizhanonline.com/job/list/>
- 可公开访问
- 按活动创建时间倒序排序

### detail：活动详情页面
- <http://www.yizhanonline.com/job/detail?id=1>
- 可公开访问
    - 方法：`GET`
    - 出错跳转最新活动页面

### apply：活动报名页面
- <http://www.yizhanonline.com/job/apply?id=1>
    - 需要有艺人登录
    - 方法：`GET`
    - 出错跳转最新活动页面

### apply_add：添加活动报名
- 在活动报名页面点击确认报名按钮
    - 需要有艺人登录
    - 方法：`POST`
    - 默认跳转活动详情页面
    - 出错跳转最新活动页面

### apply_detail：报名详情页面
- <http://www.yizhanonline.com/job/applydetail?id=1>
    - 需要有艺人登录，或有企业用户登录
    - 艺人只能查看自己的报名详情
    - 企业用户只能查看属于自己活动的报名详情
    - 方法：`GET`
    - 出错跳转最新活动页面

### apply_list：报名列表页面
- <http://www.yizhanonline.com/job/applylist?id=1>
    - 需要有企业用户登录
    - 企业用户只能查看属于自己活动的报名列表
    - 方法：`GET`
    - 出错跳转管理活动页面

### manage：管理活动页面
- <http://www.yizhanonline.com/job/manage>
    - 需要有企业用户登录

### edit：新建/编辑活动页面
- 新建：<http://www.yizhanonline.com/job/edit>
- 编辑：<http://www.yizhanonline.com/job/edit?id=1>
    - 方法：`GET`
    - 需要有企业用户登录
    - 企业用户只能修改属于自己的活动
    - 出错跳转管理活动页面

### update：添加/更新活动
- 在新建/编辑活动页面点击保存活动按钮后
    - 方法：`POST`
    - 需要有企业用户登录
    - 企业用户只能更新属于自己的活动
    - 默认跳转活动详情页面
    - 出错跳转新建/编辑活动页面
