# 逻辑层 Controller
> 位置：`Main/views/*.py`

## 首页 `index.py`
> 首页及文档

### index：网站首页
- <http://www.yizhanonline.com>

### docs：本文档
- <http://www.yizhanonline.com/docs>

******

## 测试 `test.py`
> 用于测试 message 模版是否可用，以及确定样式和显示级别

### message: 消息系统测试
- <http://www.yizhanonline.com/test/message.html>

******

## 上传 `upload.py`
> 用于处理上传文件的逻辑

### from_tinymce：富文本编辑器插入图片上传
- 在 Tinymce 富文本编辑器中插入图片时自动上传
    - 方法：`POST`/`Ajax`
    - 默认在富文本编辑器中插入图片
    - 出错时弹出 alert 框通知
- 图片分类：
    - 艺人简介图片：`'artist:description'`
        - 需要有艺人登录
    - 企业简介图片：`'enterprise:description'`
        - 需要有企业用户登录
    - 活动详情图片：`'job:description'`
        - 需要有企业用户登录
    - 未分类：`'unsorted'`
        - 可公开上传
