# 艺人 `artist.py`
> 实现所有与艺人相关的业务逻辑

- 需要有艺人登录的页面，若艺人未登录，则跳转到艺人登录/注册页面，登录/注册后跳回当前页面。

## login: 艺人登录/注册页面
- <http://www.yizhanonline.com/artist/login>

## signing_up：艺人注册逻辑
- 在 login 页面点击注册按钮后
    - 默认跳转个人中心页面，若有跳转来的页面，则跳转回去
    - 出错跳转艺人登录/注册页面

## logging_in：艺人登录逻辑
- 在 login 页面点击登录按钮后
    - 默认跳转个人中心页面，若有跳转来的页面，则跳转回去
    - 出错跳转艺人登录/注册页面

## logout：艺人登出逻辑
- 在任何页面点击导航条的登出按钮后
    - 默认跳转艺人登录页面
    - 出错跳转首页

## profile：艺人完善资料页面
- <http://www.yizhanonline.com/artist/profile/>
    - 需要有艺人登录

## profile_update：艺人提交修改的资料页面
- 在艺人完善资料页面点击保存后
    - 需要有艺人登录
    - 默认跳转上一页面，或艺人完善资料页面
    - 出错跳转艺人完善资料页面

## headimg_update：艺人修改头像
- 在艺人完善资料页面点击修改头像并提交后
    - 需要有艺人登录
    - 跳转艺人完善资料页面

## add_profile_photo：艺人上传照片
- 在艺人完善资料页面点击添加照片并提交后
    - 需要有艺人登录
    - 跳转艺人完善资料页面

## del_profile_photo：删除艺人照片
- 在艺人完善资料页面点击删除照片小按钮后
- 会检查是否为当前照片主人
    - 需要有艺人登录
    - 默认跳转上一页面，或艺人完善资料页面
    - 出错跳转艺人完善资料页面

## detail：艺人信息页面
- <http://www.yizhanonline.com/artist/detail?id=1>
    - 出错跳转首页

## dashboard：艺人个人中心页面
- <http://www.yizhanonline.com/artist/dashboard/>
    - 需要有艺人登录
