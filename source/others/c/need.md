# 需求 `need.py`
## solutionneed_detail：方案服务详情页面
- <http://www.yizhanonline.com/needs/solutionneed/detail?id=1>
    - 出错跳转首页

## solutionneed_update：提交方案服务
- 在方案服务页面点击提交方案需求按钮
    - 方法：`POST`
    - 需要有企业用户登录

## artistneed_detail：
    - 方法：`GET`
    - 可公开访问

## artistneed_edit：
    - 方法：`GET`

## artistneed_update：
    - 方法：`POST`
