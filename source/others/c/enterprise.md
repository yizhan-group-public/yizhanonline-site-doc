# 企业 `enterprise.py`
> 实现所有与企业用户相关的业务逻辑

- 需要有企业用户登录的页面，若企业用户未登录，则跳转到企业用户登录/注册页面，登录/注册后跳回当前页面。

## login: 企业用户登录/注册页面
- <http://www.yizhanonline.com/enterprise/login>

## signing_up：企业用户注册逻辑
- 在 login 页面点击注册按钮后
    - 默认跳转首页，若有跳转来的页面，则跳转回去
    - 出错跳转企业用户登录/注册页面

## logging_in：企业用户登录逻辑
- 在 login 页面点击登录按钮后
    - 默认跳转首页，若有跳转来的页面，则跳转回去
    - 出错跳转企业用户登录/注册页面

## logout：企业用户登出逻辑
- 在任何页面点击导航条的登出按钮后
    - 默认跳转企业用户登录页面
    - 出错跳转首页

## detail：企业详情页面
- <http://www.yizhanonline.com/enterprise/detail?id=1>
    - 出错跳转首页
