# 服务器
## 主服务器
- 服务器托管：阿里云 ECS
- 服务器所在地：香港
- IP：`47.90.60.235`
- root 用户密码：`A*******z`


## 备用服务器
- 服务器托管：阿里云 ECS
- 服务器所在地：华北
- IP：`120.27.117.69`
- root 用户密码：`Y*********@`

## 软件信息
### Apt-get
- 安装软件的命令：`sudo apt-get <name>`
- git、tcsh、python3-pip、apt-get update、nginx、mysql-server、curl、composer、uwsgi

### pip
- 安装命令：`sudo pip3 install <name>`
    - `sudo pip3 install -r requirements.pip`
- Django、pymysql、KyanToolKit、consoleiotools、Django-cleanup

## 软件配置
### nginx
- nginx conf：`/etc/nginx/sites-available`
- nginx 命令：
```sh
sudo nginx start  # start
sudo nginx -s reload  # reload confs
sudo nginx -s stop  # stop
sudo nginx -t  # test confs
```

### uwsgi
- uwsgi conf: `uwsgi.xml`
- uwsgi script: `uwsgiTool.py`
```sh
./uwsgiTool.py  # choose your action
./uwsgiTool.py start  # start
./uwsgiTool.py reload  # reload codes
./uwsgiTool.py stop  # stop
```

### mysql
- root 密码：`y*********l`
- Django 用 Mysql 用户：`yizhan`:`y*********l`
- 数据库名称：`yizhandb`

### Django
- admin 用户：`yizhan`:`y*********l`
- admin 地址：<http://www.yizhanonline.com/admin>
```sh
python3 manage.py runserver  # start a local server at 127.0.0.1:8000
python3 manage.py makemigrations  # after modified models
python3 manage.py migrate  # apply updated migrations to MySql
```

### memcache
```
sudo apt-get install memcached
sudo pip3 install python-memcached
```
- 地址：127.0.0.1:11211

### mkdocs
- 安装命令：`sudo pip3 install mkdocs`
- 运行命令：`mkdocs serve`
- 生成 html 命令：`mkdocs build`


# 版本库
- 类型：Git
- 服务商：Gitlab
- 所有人：yizhan-group
- 库地址：<https://gitlab.com/yizhan-group/yizhanonline-site.git>

# 代码结构
- （m）数据模型：`main/models.py`
- （v）页面模版：`main/templates/*.html`
- （c）业务逻辑：`main/views/*.py`
- Url router: `yizhan/urls.py`
