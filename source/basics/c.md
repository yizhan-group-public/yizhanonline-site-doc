# Controller (Views)
## util/context_processors.py
> 用来自动往 request context 中注入已登录的艺人和企业用户信息

```
Args:
    request: Django HttpRequest object
Returns:
    {
        'lartist': logged_artist,
        'lenterprise': logged_enterprise,
    }
```

- in `settings.py`：
```py
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                ...,
                'util.context_processors.logged_user',
            ],
        },
    },
]
```

******

## util/artist.py
> 提供常用的 artist/enterprise 工具

```py
util.artist.get_current_user(request)  # 获得当前登录的艺人用户信息
util.artist.login_to_continue(request)  # 显示提示信息，并跳转至艺人登录/注册页面，登录/注册后跳转回来
util.artist.remember_login(request, artist)  # 记住当前登录的艺人用户，关闭浏览器也不会丢失
```

******

## util/enterprise.py
```py
util.enterprise.get_current_user(request)  # 获得当前登录的企业用户信息
util.enterprise.login_to_continue(request)  # 显示提示信息，并跳转至企业用户登录/注册页面，登录/注册后跳转回来
util.enterprise.remember_login(request, enterprise)  # 记住当前登录的企业用户，关闭浏览器也不会丢失
```

******

## util/filetool.py
> 提供对图片储存的相关工具

```py
util.filetool.save_img(imagefile, filepath)  # 富文本编辑器用即时储存图片函数
util.filetool.ensure_dir(path)  # 保证路径存在，一般用于 save_img 内部调用
```

******

## util/formats.py
> 用于转化 date/datetime/timedelta 时间格式为可读格式

```py
util.formats.formatDate(dt, mode="optimize")  # datetime 转化
    # mode == 'optimize':  # [2015-]12-05 11:25 am
    # mode == 'dateonly':  # 12-05
    # mode == 'fulldateonly':  # 2015-12-05
    # mode == 'timeonly':  # 11:25:59
    # default:  # 2015-12-05 11:25 am
```

```py
util.formats.formatTimedelta(td, mode="full")  # timedelta 转化
    # mode == 'full':  # 1天 11小时 7分 22秒
    # mode == 'largest':  # 1天 / 11小时 / 7分 / 22秒
```
