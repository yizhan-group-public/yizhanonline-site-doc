# Views（templates）
## 基本模版 `base.html`
- 定义了通用的 html 头
- 引入了通用的第三方库，Bootstrap3 和 jQuery
- 对所有页面适用的 css 样式和 js 脚本
- 默认引入 Message 模块
- 默认引入“返回顶部”按钮

### 使用方法
```
{% extends 'base.html' %}

{% block title %}这里写页面标题{% endblock %}

{% block css %}
    <style>
        <!-- 这里写 css 代码 -->
    </style>
{% endblock css %}

{% block js %}
    <script>
        // 这里写 js 代码
    </script>
{% endblock %}

{% block inhead %}
    <!-- 这里引用额外的 css js 文件 -->
{% endblock %}

{% block content %}
    <!-- 这里写 html body 代码 -->
{% endblock %}
```

******

## 页脚模版 `footer.html`
- 即页面最下方网站信息栏
- 使用方法：
```
{% include "footer.html" %}
```

******

## 导航条模版 `navbar.html`
- 定义了导航条的基础框架，包括样式和艺人、企业按钮。
- 当艺人用户已登录时：下拉菜单显示个人中心、修改资料、艺人登出按钮
- 当企业用户已登录时：下拉菜单显示企业详情、新建活动、管理活动、企业登出按钮
- 当无任何用户登录时：显示艺人登录、企业用户登录按钮
- 添加导航连接：
```
{% block navlink %}
    <li><a href="link1">link1</a></li>
    <li><a href="link2">link2</a></li>
{% endblock %}
```
- 使用方法：
```
{% include 'navbar.html' %}
```

******

## 消息部件 `messages.html`
- 用于显示系统给用户的未读消息
- 会从后台取回所有未读消息，并逐条显示
- 根据不同的消息类型，显示不同的消息样式
    - 在 `settings.py` 中，将 ERROR 级别的消息影射成 danger
- 取消方法
```
{% block messages %}{% endblock %}
```
- 消息测试页面：<http://www.yizhanonline.com/test/message.html>
