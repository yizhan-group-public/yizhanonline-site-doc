# Models
## 通用数据抽象类
```py
class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
```
- BaseModel 继承了 Django 自带的 models.Model 类
- 为所有子类增加了 2 个通用属性：created、modified。
    - created 是在数据库新增一条数据时自动设定为当时的时间
    - modified 是在一条数据有修改时自动更新为当时的时间

******

## 通用需求数据抽象类
```py
class NeedsModel(BaseModel):
    enterpriseid = models.IntegerField()
    contactname = models.CharField(max_length=255, default='', blank=True)
    email = models.CharField(max_length=255, default='', blank=True)
    phone = models.CharField(max_length=255, default='', blank=True)
    city = models.CharField(max_length=255, default='', blank=True)
    eventname = models.CharField(max_length=255, default='', blank=True)
    eventlocation = models.CharField(max_length=255, default='', blank=True)

    @property
    def enterprise(self):
        """Get the enterprise who created this need"""
        return Enterprise.objects.get(id=self.enterpriseid)

    class Meta:
        abstract = True
```
- NeedsModel 是为需求类模型的父模版。
- 所有的需求都会有企业id、联系人姓名、邮箱、电话、需求所在城市、活动的名称和地点这些属性。
- 所有需求都可以通过 .enterprise 直接访问需求所属的企业。
